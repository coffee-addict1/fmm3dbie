EXEC = int2-ext
HOST = gcc-openmp

#
# For linux systems, it is assumed that the environment
# variable LD_LIBRARY_PATH contains the locations to libfmm3d.so
# and libsolvers3d.so, for Macosx, these .so files also need to be
# copied over /usr/local/lib

ifneq ($(OS),Windows_NT) 
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Darwin)
        LDF = /usr/local/lib
        DYLD_LIBRARY_PATH = ${LDF}
        LD_LIBRARY_PATH = ${LDF}
        DOCKER_DIR = /home/user/lib
    endif
    ifeq ($(UNAME_S),Linux)
        LDF = ${HOME}/lib
    endif
endif

LIBS = -lfmm3d -lfmm3dbie 
ifeq ($(HOST),gcc)
    FC=gfortran -L${LDF} 
    FFLAGS=-fPIC -O3 -funroll-loops -march=native -std=legacy 
endif

ifeq ($(HOST),gcc-openmp)
    FC = gfortran 
    FFLAGS=-fPIC -O3 -funroll-loops -march=native -fopenmp -std=legacy 
endif

ifeq ($(HOST),intel)
    FC=ifort -L${LDF} 
    FFLAGS= -O3 -fPIC -march=native
endif

ifeq ($(HOST),intel-openmp)
    FC = ifort -L${LDF} 
    FFLAGS= -O3 -fPIC -march=native -qopenmp
endif

SURF=../../src/surface_routs

.PHONY: all clean 

OBJECTS = lap_dir_ext_example_tori.o \

%.o : %.f90  
	$(FC) -c $(FFLAGS) $< -o $@

all: $(OBJECTS)
	$(FC) $(FFLAGS) -o $(EXEC) $(OBJECTS) -lfmm3dbie -lfmm3d
	./$(EXEC)  

## gdb with Docker. need to update to make this work
#gdb: $(OBJECTS) 
#  docker run --rm -it -v $(PWD):/home/usr/fmm3dbie -w /home/user/fmm3dbie fmm3dbie /bin/bash -c \
#  --security-opt seccomp=unconfined \
#  "$(FC) $(FFLAGS) -o $(EXEC) $(OBJECTS) -L${DOCKER_DIR} -lfmm3dbie \
#  -L${DOCKER_DIR} -lfmm3d -ggdb"

clean:
	rm -f $(OBJECTS)
	rm -f $(EXEC)

list: $(SOURCES)
	$(warning Requires:  $^)

